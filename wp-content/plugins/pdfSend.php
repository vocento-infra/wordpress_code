<?php
/*
Plugin Name: pdfSend
Plugin URI: https://www.vocento.com
Description: Read /pdf from /wp-content/uploads/
Version: 0.0.1
Author: Uxio Faria
Text Domain: pdfSend
*/


add_action( 'parse_request', function( $wp ) {
        if ( preg_match( '/^pdf(\/.*\.pdf)$/', $wp->request, $matches ) ) {
                $pdf = $matches[1];
                $upload = wp_upload_dir();
                $size = filesize($upload['basedir'] . $pdf);
                header("Content-Type: application/pdf");
                header("content-Length: $size");
                echo file_get_contents($upload['basedir'] . $pdf);
                exit;
        }
});

?>
